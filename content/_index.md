---
title: "Recording the History of Information Security Knowledge"
description: "A site of articles to share the history of information security"
---

# Welcome

This project is intended to be a place to collect the knowledge of Information Security that has grown over the decades the discipline has started and grown. It is based on a few simple philosophies:

-  Knowledge is in small consumbale articles that discuss a very specific event related to information security knowledge (exampless: first discovery of a type of security flaw, announcement of a specific break, a set of news articles about a specific publicized incident)
-  Articles are linked together by tags which may be numerous for a specific article. For example, an article about Aleph One's `Smashing the Stack for Fun and Profit` might have tags of `Aleph One`, `BugTraq`, `Phrack`, `Phrack 49`, `r00t`, `Underground.org`, `Phrack v7 i49 f14`, `buffer overflow`, `stack smashing`.
-  Each tag will have a page, each topic should have at least one article.
-  As additional tags are determined to be a good fit to combine knowledge into a page of articles where an article should be included, it would get more tags added to it which would then include it in those pages.
-  Articles are combined together via tag and presented in timeline order

ToDo: Figure out a way to have an introductory article for each tag/topic

Contribute to this via the instructions at the [Rhisk Project](https://gitlab.com/rhisk/rhisk.gitlab.io)

This website is powered by [GitLab Pages](https://about.gitlab.com/features/pages/)
and [Hugo](https://gohugo.io), and can be built in under 1 minute.
w
