---
type: page
menu: footer
title: Contributing
---

**_ NOTE: This is all a huge draft, and your ideas and contributions are encouraged _**

In order to contribute please clone this repository and provide a pull request with all of your changes.

Currently accepting most enhancements that can be verified via the provided references.

### Philosophy

The base unit of information in this project is an article. Each article should be self-contained in content. Each article should be about a specific event in the timeline of knowledge about a topic.
Some examples of this include a paper or presentation about a specific topic. A specific breach announcement which includes how it happened. Avoid using terms like "firt" or "initial", the timeline will speak for itself as what came first, as more content is added.

### Articles

An article includes the following information:

-  Author & Contributors
   -  Who has worked on creating or updating this particular article
   -  Currently add your name/handle to the list of authors
   -  Hopefully this can be automated in the future from the commit history
-  When
   -  This should be in a timestamp format
   -  Largest acceptable period is be a month and a year: 2022-04
   -  Smallest acceptable period is year, month, day, hour, minute, second
   -  If a time has valid hours, minutes, or seconds then it must also have a timezone
-  Tags
   -  Articles will be related to each other via tags
   -  A page will be generated for each tag created, including all of the articles with a shared tag in when order
-  References
   -  Where is this information able to be verified
   -  URLs are acceptable, preferrably to a perpetual site which will maintain this information
      -  archive.org
      -  Wikipedia
      -  Site postings (ex breach announcements)
      -  News articles (usually)
-  Content
   -  This is the content of the article, where you can provide the facts and details about the particular knowledge
      this article provides.
   -  Think of an article as a single paragraph of a very specific area of knowledge, without including other areas
   -  Each article should be an encapasulation of a specific aspect of knowledge, for example:
      -  Aleph One's "Smashing the Stack for Fun and Profit"
      -  Information about the first stack canary
      -  Information about defeating a stack canary
      -  Information about the creation of ASLR

## How to add to the Knowledge

Right now the method is kinda geeky, and may not be great for all people.
We're thinking about how to remove some of this barrier in the future to make it easier for all to contribute (without going the full wiki method)

Data files are preferred to be in YAML format, but JSON is also acceptable (anything that can have a verifiable "pretty" state which is readable)

ToDo: create and add a data validation step for the CI/CD pipeline

### How To - Adding to existing Tags

-  Determine what you'd like to contribute
-  Do your research for your article
-  Run `hugo serve` to show your changes
-  Add your content (see above) in the `data/` directory
-  Make sure it has all the required elements
-  PR your changes

### How To - New Tags

-  Determine what you'd like to contribute
-  Do your research for your article
-  Run `hugo serve` to show your changes
-  Add your content in the `data/` directory
-  Create the page for the new tags you included
-  PR your changes
