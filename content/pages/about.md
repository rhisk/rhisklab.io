---
title: "About"
type: page
menu:
   - footer
---

A collection of articles related to specific topics in InformatioN Security. The goal is to collect enough information to be able to create timelines of knowledge in easily consumable chunks. Each article should be self contained and only reference other article as needed and
the relationship is documented. Example: a paper directly references another paper or event.
