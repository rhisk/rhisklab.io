---
title: "SQLi - Rain Forest Puppy"
Tags: [sqli, "sql injection", Phrack, "Prack 54"]
tags: [
      "sqli overflow",
      "rain forest puppy",
      rfp
      Phrack,
      "Phrack Magazine",
      "Phrack 54",
      "Phrack volume 8 issue 54 article 08",
      sqli,
   ]
authors: ["Ricky"]
date: 1998-12-25
references: ["http://phrack.org/issues/54/8.html"]
---

In an article titled "NT Web Technology Vulnerabilities", Rain Forest Puppy displays examples of
corrupting sql statements with user input.
