---
title: Smashing The Stack For Fun And Profit
tags:
   [
      "buffer overflow",
      "Aleph1",
      phrack,
      "phrack 49",
      "Phrack Magazine",
      "phrack v7 i49 f14",
      rce,
      "remote code execution",
   ]
authors: ["Ricky"]
date: 2022-11-19
references:
   [
      "http://phrack.org/issues/49/1.html",
      "https://inst.eecs.berkeley.edu/~cs161/fa08/papers/stack_smashing.pdf",
   ]
when: 1996-11-08
---

The first article published on how to perform a buffer overflow aka "smashing the stack". Aleph One disclosed how memory is allocated inside a C (and other) programs, and a way to overwrite memory. Using this method you can have the computer run code that you provide.
